from datetime import datetime
from threading import Thread
from multiprocessing import Process


def count_lucky_ticket(start_number, end_number):
    count = 0
    for i in range(start_number, end_number):
        num = str(i).rjust(6, "0")
        if int(num[0]) + int(num[1]) + int(num[2]) == int(num[3]) + int(num[4]) + int(num[5]):
            count += 1
    print('Amount of lucky tickets:', count)
    return count


if __name__ == '__main__':
    start = datetime.now()
    count_lucky_ticket(1, 999999)
    end = datetime.now()
    print(f'Time in one thread: {end - start}')

    thread1 = Thread(target=count_lucky_ticket, args=(1, 500000))
    thread2 = Thread(target=count_lucky_ticket, args=(500001, 999999))
    start = datetime.now()
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    end = datetime.now()
    print(f'Time in two threads: {end - start}')

    process1 = Process(target=count_lucky_ticket, args=(1, 500000))
    process2 = Process(target=count_lucky_ticket, args=(500001, 999999))
    start = datetime.now()
    process1.start()
    process2.start()
    process1.join()
    process2.join()
    end = datetime.now()
    print(f'Time in two processes: {end - start}')
